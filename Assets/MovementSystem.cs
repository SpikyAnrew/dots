﻿using Unity.Burst;
using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Transforms;
using UnityEngine;
using static Unity.Mathematics.math;

public class MovementSystem : JobComponentSystem
{
    // This declares a new kind of job, which is a unit of work to do.
    // The job is declared as an IJobForEach<Translation, Rotation>,
    // meaning it will process all entities in the world that have both
    // Translation and Rotation components. Change it to process the component
    // types you want.
    //
    // The job is also tagged with the BurstCompile attribute, which means
    // that the Burst compiler will optimize it for the best performance.
    [BurstCompile]
    struct MovementSystemJob : IJobForEach<Translation, MovementSpeed>
    {
        // Add fields here that your job needs to do its work.
        // For example,
        public float deltaTime;
        
        
        
        public void Execute(ref Translation translation, ref MovementSpeed movementSpeed)
        {
            // Implement the work to perform for each entity here.
            // You should only access data that is local or that is a
            // field on this job. Note that the 'rotation' parameter is
            // marked as [ReadOnly], which means it cannot be modified,
            // but allows this job to run in parallel with other jobs
            // that want to read Rotation component data.
            // For example,
            //     translation.Value += mul(rotation.Value, new float3(0, 0, 1)) * deltaTime;
            translation.Value += movementSpeed.velocity * deltaTime;
            
            float beta = deltaTime;
            float x = movementSpeed.velocity.x;
            float y = movementSpeed.velocity.y;
            float sinus = sin(beta);
            float cosinus = cos(beta);
            movementSpeed.velocity.x = cosinus*x - sinus*y;
            movementSpeed.velocity.y = sinus*x + cosinus*y;
        }
    }
    
    protected override JobHandle OnUpdate(JobHandle inputDependencies)
    {
        var job = new MovementSystemJob {deltaTime = Time.deltaTime};

        // Assign values to the fields on your job here, so that it has
        // everything it needs to do its work when it runs later.
        // For example,


        // Now that the job is set up, schedule it to be run. 
        return job.Schedule(this, inputDependencies);
    }
}