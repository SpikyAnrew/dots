﻿using System;
using System.Collections;
using System.Collections.Generic;
using Unity.Collections;
using UnityEngine;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Rendering;
using Unity.Transforms;
using UnityEngine.Serialization;
using Random = UnityEngine.Random;

public class Testing : MonoBehaviour
{
    [SerializeField] private Mesh mesh;
    [SerializeField] private Material material;
    [FormerlySerializedAs("numberOfParticles")] [SerializeField] private int particlesPerSecond;
    [SerializeField] private float minVelocity;
    [SerializeField] private float maxVelocity;

    // Start is called before the first frame update
    void Update()
    {
        var em = World.Active.EntityManager;
        NativeArray<Entity> entities = new NativeArray<Entity>((int)(particlesPerSecond * Time.deltaTime) , Allocator.Temp);
        var archetype = em.CreateArchetype(
            typeof(Level),
            typeof(Translation),
            typeof(RenderMesh),
            typeof(LocalToWorld),
            typeof(MovementSpeed)
        );
        em.CreateEntity(archetype, entities: entities);  
        foreach (var entity in entities)
        {
            float angle = Random.Range(-5, 5);
            float v = Random.Range(minVelocity, maxVelocity);
            em.SetComponentData(entity, new MovementSpeed()
            {
                velocity = new float3(math.sin(angle)*v, math.cos(angle)*v, 0f)
            });
            em.SetSharedComponentData(entity, new RenderMesh()
            {
                mesh = mesh,
                material = material
            });
        }
        
    }
}
